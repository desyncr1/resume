resume.pdf: resume.tex
	xelatex resume.tex

install:
	brew install basictex
	tlmgr install enumitem xifthen ifmtarg

clean:
	rm *.log *.aux *.out

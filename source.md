Afluenta
Technical Team Leader
Buenos Aires
May 2016 – Present
Mentor and guide engineers ensuring technical and security best practices are understood and followed
Improve technical and product team collaboration through Agile methodologies and tightening communication
Lead efforts to efficiently deconstruct a complex monolithic and legacy platform to an API-first and micro-service based architecture
Work closely with support units (HR, Operations) to improve hiring and on-boarding processes increasing retention rates up to 80%
Improve platform stability and reduce outtages by introducing and deploying a Continuous Integration pipeline
Execute team and 1-to-1 performance and career development discussions improving team retention and collaboration
Lead research and implementation of new technologies, lead technical talks and training efforts (AWS, Kubernetes, REST)

Wobiz
Senior Software Developer
Buenos Aires
February 2015 – April 2016
Introduced style guides and code conventions as development team double its size increased team speed as a result
Organized and re-structured QA and development team collaboration to improve communication and reduce the feedback loop
Transformed a legacy code base into multiple, testable and easy to reason blocks reducing the learning curve
Created unit and end-to-end testing practices and tooling for the main product reducing bugs and increasing development speed

FWTV
Senior Developer
Buenos Aires
September 2014 – February 2015
Built a deployment and continuous integration pipeline on our cloud-based infrastructure
Increased development speed and team collaboration by mentoring and pair-programming
Lead automation, testing and back-end development working alongside team leaders and product team

SaludMovil
Technical Leader
Buenos Aires
November 2013 – September 2014
Researched and built solutions to architectural and technical challenges with strong focus on re-usability, scalability and security
Built efficient, reusable and flexible components and libraries for our code product
Developed open source components for popular frameworks and introduced practices to contribute to open source software
Created automated deployment tools, multi-stage environment and continuous integration setup

DosAlCubo S.A.
Backend Developer
Buenos Aires
January 2012 – November 2013
Used agile methodologies to meet deadlines and ensure product quality through the QA team
Worked with a host of well-established technologies including PHP and MySQL

PHP Developer Trainee
La Plata
June 2011 – August 2011
Worked with PHP, MySQL and Drupal 6 and QA reporting tools DevTrack and DevTest to create and follow bug reports
Used Phing for building and deploying and PHPUnit for testing following the TDD software development methodology
